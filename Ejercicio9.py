# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)

#Entrada
lista= list([int(s) for s in input().split()])
#Proceso
max, min = lista.index(max(lista)), lista.index(min(lista))
lista[max], lista[min] = lista[min], lista[max]
#Salida
print(lista)

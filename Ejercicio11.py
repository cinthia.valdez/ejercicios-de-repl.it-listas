# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
for c in range(0, len(a)):
  if a.count(a[c]) == 1:
    print(a[c])
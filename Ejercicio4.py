# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)
entrada = input()
lst = list(map(int, entrada.split()))
c = 1
for c in range(1, len(lst)):
  if lst[c] * lst[c-1] > 0:
    print(str(lst[c - 1]), str(lst[c]))
    break
  elif c == len(lst)-1:
    print("0")
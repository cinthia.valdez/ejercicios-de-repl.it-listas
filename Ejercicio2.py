# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
for numeros_pares in a:
  if numeros_pares%2==0:
    print(numeros_pares)

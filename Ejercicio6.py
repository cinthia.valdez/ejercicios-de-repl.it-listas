# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)
#Entrada
lista = [int(s) for s in input().split()]
"""print("CONTENIDO ACTUAL DE LALISTA ES:", lista)"""
frecuencia={}
#Proceso
for n in lista:
  if n in frecuencia:
    frecuencia[n] +=1
  else:
    frecuencia[n]=1
#Salida
"""print("contenido actual de el diccionario frecuencia :", frecuencia)"""
print(len(frecuencia))
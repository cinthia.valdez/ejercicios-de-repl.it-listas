# Read a list of integers:
# Print a value:
# print(a)
# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Entrada
a = [int(s) for s in input().split()]
"""print"(a)"""
indice = 0
# Proceso
for i in a:
    if indice % 2 == 0:
# Salisa
        print(i)
    indice += 1

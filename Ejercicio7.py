# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)
lista= [int(s) for s in input().split()]
for i in range(0, len(lista), 2):
    val = lista.pop(i)
    lista.insert(i + 1, val)
print(lista)
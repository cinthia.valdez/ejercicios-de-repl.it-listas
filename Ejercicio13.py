# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)
N, K = [int(s) for s in input().split()]
game= ["I"] * N
for i in range(K):
  left, right = [int(s) for s in input().split()]
  for j in range(left -1, right):
    game[j]= "."
print("".join(game))
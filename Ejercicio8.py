# Cinthia Valdez
# email: cinthia.valdez@unl.edu.ec
# Read a list of integers:
# Print a value:
# print(a)

#Entrada
lista = [int(s) for s in input().split()]
alto = lista[0]
#Proceso
for n in range(0, (len(lista))):
  if lista[n]>alto:
    alto= lista[n]
#Salida
print(alto, lista.index(alto))
